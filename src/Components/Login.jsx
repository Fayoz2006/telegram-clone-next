import Head from 'next/head'
import React from 'react'
import Image from 'next/image'
import Logo from "../../public/images/logo.png"
import GoogleLogo from '../../public/images/google-logo.png'
import { signInWithPopup } from 'firebase/auth'
import { auth, provider } from '../../firebase'

const Login = () => {
    const login = async () => {
        await signInWithPopup(auth, provider)
    }

    return (
        <>
            <Head>
                <title>Telegram</title>
                <link rel="shortcut icon" href="https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Telegram_logo.svg/768px-Telegram_logo.svg.png" type="image/x-icon" />
            </Head>
            <main className='w-full h-screen overflow-hidden bg-[#242424] flex flex-col items-center justify-center space-y-10'>
                <div className='flex items-center space-x-4'>
                    <Image src={Logo} width={100} height={100} property="true" quality={100} alt="logo" />
                    <h1 className='text-4xl font-extrabold'>Telegram Web</h1>
                </div>
                <button onClick={login} className='flex items-center text-3xl bg-white text-black px-5 py-2 rounded-md hover:bg-[#e2e2e2] transition-colors font-semibold space-x-3' >
                    <Image src={GoogleLogo} width={40} height={40} property="true" quality={100} alt="google" />
                    <span>Login with google</span>
                </button>
            </main>
        </>
    )
}

export default Login
