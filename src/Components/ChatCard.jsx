import React from 'react'
import Link from 'next/link'
import Image from 'next/image'
import DefaultImage from '../../public/images/default.png'
import { auth, db } from '../../firebase'
import { useAuthState } from 'react-firebase-hooks/auth'
import { useCollection } from 'react-firebase-hooks/firestore'
import { collection, query, where } from 'firebase/firestore'

const ChatCard = ({ item }) => {
    const [user, loading] = useAuthState(auth)
    const reciverEmail = item?.data()?.users?.filter(item => item !== user?.email)?.[0]
    const usersRef = collection(db, 'users')
    const q = query(usersRef, where(`email`, '==', reciverEmail))
    const [userSnapShot, loading2] = useCollection(q)
    const name = userSnapShot?.docs?.[0]?.data()?.name
    const imageURL = userSnapShot?.docs?.[0]?.data()?.imageURL
    const online = userSnapShot?.docs?.[0]?.data()?.online

    return (
        <Link href={`/chats/${item?.id}`}>
            <div className='w-full flex items-center space-x-6 py-3 px-5 border-b rounded-[4px] border-[#1f1f1f] cursor-pointer hover:bg-[#1d1d1d]'>
                <div className='border rounded-full w-[55px] h-[55px] relative'>
                    <Image src={imageURL || DefaultImage} width={55} height={55} quality={100} property="true" className='rounded-full' alt="" />
                    {
                        online ? <span className='w-2.5 h-2.5 bg-green-500 rounded-full absolute z-[2] bottom-0 right-[4px]'></span> : ""
                    }
                </div>
                <span>{name || "user"}</span>
            </div>
        </Link>
    )
}

export default ChatCard
