import React from 'react'

const Message = ({ msg }) => {
    const message = msg?.data()?.message
    const createdAt = msg?.data()?.createdAt
    const newDate = new Date(createdAt?.seconds * 1000)
    const time = newDate.toLocaleTimeString().slice(0, 5)
    return (
        <div className='max-w-[200px] bg-gradient-to-r from-sky-700 to-indigo-500 p-2.5 rounded-b-xl rounded-tr-xl cursor-pointer'>
            {message}
            <div className='w-full text-end text-[12px]'>{time}</div>
        </div>
    )
}

export default Message