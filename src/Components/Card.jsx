import React from 'react'
import Image from 'next/image'
import { auth, db } from '../../firebase'
import { doc, setDoc } from 'firebase/firestore'
import { useAuthState } from 'react-firebase-hooks/auth'

const Card = ({ item, id, setSearch }) => {
    const [user, loading] = useAuthState(auth)
    const addChat = async () => {
        await setDoc(doc(db, "chats", `chats-${user?.uid}${id}`), {
            users: [user?.email, item.email]
        }, { merge: true });
        setSearch("")
    }
    return (
        <div className='w-full flex items-center space-x-6 py-3 px-5 border-b rounded-[4px] border-[#1f1f1f] cursor-pointer hover:bg-[#1d1d1d]' onClick={addChat}>
            <div className='border rounded-full w-[55px] h-[55px] overflow-hidden'>
                <Image src={item.imageURL} width={55} height={55} quality={100} property="true" alt="" />
            </div>
            <span>{item.name}</span>
        </div>
    )
}

export default Card
