import React, { useState } from 'react'
import Card from './Card'
import Image from 'next/image'
import ChatCard from './ChatCard'
import CardLoader from './CardLoader'
import { FiMenu } from 'react-icons/fi'
import { signOut } from 'firebase/auth'
import { auth, db } from '../../firebase'
import { IoMdClose } from 'react-icons/io'
import { useAuthState } from 'react-firebase-hooks/auth'
import { useCollection } from 'react-firebase-hooks/firestore'
import { AiOutlineSearch } from 'react-icons/ai'
import { setDoc, doc, collection, query, where, serverTimestamp } from 'firebase/firestore'

const Sidebar = () => {
  const [menu, setMenu] = useState(false)
  const [search, setSearch] = useState("")
  const [user, loading] = useAuthState(auth)

  const logout = async () => {
    if (user) {
      await setDoc(doc(db, "users", user?.uid), {
        name: user?.displayName,
        email: user?.email,
        imageURL: user?.photoURL,
        online: false,
        lastSeen: serverTimestamp()
      }, { merge: true });
    }
    await signOut(auth)
  }

  const usersRef = collection(db, 'users')
  const [userSnapShots, loading2] = useCollection(usersRef)

  const chatsRef = collection(db, 'chats')
  const q = query(chatsRef, where(`users`, `array-contains`, user?.email));
  const [chatSnapShots, loading3] = useCollection(q)

  return (
    <div className='w-[600px] h-screen p-3 bg-[#272727] relative'>
      <div className='flex items-center w-full space-x-4'>
        <div className='flex items-center'>
          <button className='text-2xl' onClick={() => setMenu(!menu)}>
            <FiMenu alt="menu" />
          </button>
          <div style={{ opacity: menu ? 1 : 0, left: menu ? '16px' : `-100%`, transition: '500ms ease' }} className='w-[90%] rounded-[12px] absolute bg-[#4f4f4f92] backdrop-blur flex flex-col justify-center items-center p-5 top-16 z-10      '>
            <div className="w-[100px] h-[100px] border-2 rounded-full overflow-hidden">
              <Image src={user?.photoURL} width={100} height={100} priority={true} quality={100} alt="user" />
            </div>
            <div className='w-full'>
              <h1 className='text-xl text-center my-2'>{user?.displayName}</h1>
              <div className='text-xl text-center bg-[#fff] w-full text-black py-1 rounden-md hover:bg-gradient-to-r from-[#EC9F05] to-[#FF4E00] hover:text-white transition-colors rounded-md cursor-pointer' onClick={logout}>Logout</div>
            </div>
          </div>
        </div>
        <div className='relative w-full flex items-center'>
          <div className='absolute text-xl left-3'>
            <AiOutlineSearch alt="search" />
          </div>
          <input onChange={(e) => setSearch(e.target.value)} value={search} className='w-full border bg-transparent px-9 py-[5px] rounded-full border-[#494949] focus:border-[#cd71ff]' placeholder='Search Here' type="text" />
          {
            search.length > 0 && <button onClick={() => setSearch("")} className='absolute right-3 text-xl'>
              <IoMdClose />
            </button>
          }
        </div>
      </div>
      <div className={search.length > 0 ? 'w-full h-screen overflow-y-auto scroll-bar mt-5 transition-all' : 'w-full h-0 overflow-y-auto mt-5 transition-all'}>
        {!loading2 ?
          userSnapShots?.docs?.map((item) => {
            if (item.data().name.toLowerCase().includes(search.toLowerCase()) && item.data().name !== user?.displayName) {
              return (
                <Card key={item.id} item={item?.data()} id={item.id} setSearch={setSearch} />
              )
            }
          })
          : <div>
            <CardLoader />
            <CardLoader />
            <CardLoader />
          </div>
        }
      </div>
      <div className='w-full h-screen overflow-y-auto scroll-bar mt-2 transition-all'>
        {
          !loading3 ? chatSnapShots?.docs?.map((item) => {
            return (
              <ChatCard key={item.id} item={item} />
            )
          })
            : <div>
              <CardLoader />
              <CardLoader />
              <CardLoader />
            </div>
        }
      </div>
    </div>
  )
}

export default Sidebar
