import React from 'react'
import Image from 'next/image'
import Loader from '../../public/gifs/loading.gif'

const Loading = () => {
    return (
        <div className='w-full h-screen flex item-center flex-col justify-center overflow-hidden bg-[#161621]'>
            <Image className='w-full' src={Loader} priority={true} quality={100} alt="loading" />
        </div>
    )
}

export default Loading
