import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth, GoogleAuthProvider } from "firebase/auth";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyCZSzsGBLGq04UnsWqAeUZ5WHXP-Yhz4RE",
    authDomain: "telegram-clone-next.firebaseapp.com",
    projectId: "telegram-clone-next",
    storageBucket: "telegram-clone-next.appspot.com",
    messagingSenderId: "656039041870",
    appId: "1:656039041870:web:7b87fe5b5e37241107dc1f"
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth(app);
export const provider = new GoogleAuthProvider(app);
export const storage = getStorage(app);