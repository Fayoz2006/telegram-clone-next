import '../styles/globals.css'
import Head from 'next/head'
import Login from '../src/Components/Login'
import Sidebar from '../src/Components/Sidebar'
import Loading from '../src/Components/Loading'
import { auth, db } from '../firebase'
import { useEffect } from 'react'
import { useAuthState } from "react-firebase-hooks/auth"
import { doc, serverTimestamp, setDoc } from "firebase/firestore";

function MyApp({ Component, pageProps }) {
  const [user, loading] = useAuthState(auth)

  useEffect(() => {
    window.addEventListener(`beforeunload`, function (e) {
      let confirmationMessage = "Do you want to leave?";
      (e || window.event).returnValue = confirmationMessage
      if (user) {
        setDoc(doc(db, "users", user?.uid), {
          name: user?.displayName,
          email: user?.email,
          imageURL: user?.photoURL,
          online: false,
          lastSeen: serverTimestamp()
        }, { merge: true });
      }
    })
    if (user) {
      setDoc(doc(db, "users", user?.uid), {
        name: user?.displayName,
        email: user?.email,
        imageURL: user?.photoURL,
        online: true,
        lastSeen: serverTimestamp()
      }, { merge: true }
      );
    }
  })

  if (loading) return <Loading />
  if (!user) return <Login />

  return (
    <>
      <Head>
        <title>Telegram</title>
        <link rel="shortcut icon" href="https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Telegram_logo.svg/768px-Telegram_logo.svg.png" type="image/x-icon" />
      </Head>
      <div className='flex'>
        <Sidebar />
        <Component {...pageProps} />
      </div>
    </>
  )
}

export default MyApp
