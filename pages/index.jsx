import Head from 'next/head'

export default function Home() {
  return (
    <>
      <Head>
        <title>Telegram</title>
        <link rel="shortcut icon" href="https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Telegram_logo.svg/768px-Telegram_logo.svg.png" type="image/x-icon" />
      </Head>
      <div className='gradient w-full h-screen overflow-hidden'></div>
    </>
  )
}
