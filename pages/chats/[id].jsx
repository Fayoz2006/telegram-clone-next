import Image from 'next/image'
import Message from '../../src/Components/Message'
import DefaultImage from '../../public/images/default.png'
import React, { useState } from 'react'
import { MdSend } from 'react-icons/md'
import { auth, db } from '../../firebase'
import { doc, getDoc } from "firebase/firestore";
import { useAuthState } from 'react-firebase-hooks/auth'
import { useCollection } from 'react-firebase-hooks/firestore'
import { collection, addDoc, serverTimestamp, query, orderBy, where } from "firebase/firestore";

export async function getServerSideProps(context) {
  const id = context.query.id
  const docRef = doc(db, "chats", id);
  const docSnap = await getDoc(docRef);
  const chatData = JSON.stringify(docSnap?.data())
  return {
    props: { id, chatData },
  }
}

const ID = ({ id, chatData }) => {
  const [user, loading] = useAuthState(auth)
  const [message, setMessage] = useState("")

  const messagesRef = collection(db, 'messages')
  const q = query(messagesRef, orderBy("createdAt"))
  const [messageSnapshots, loading2] = useCollection(q)

  const createMessage = async (event) => {
    event.preventDefault()
    const docRef = await addDoc(collection(db, "messages"), {
      message: message,
      user: user?.email,
      chatId: id,
      createdAt: serverTimestamp()
    });
    setMessage("")
  }

  const data = JSON.parse(chatData)
  const reciverEmail = data?.users?.filter(item => item !== user?.email)?.[0]
  const usersRef = collection(db, 'users')
  const q2 = query(usersRef, where(`email`, '==', reciverEmail))
  const [userSnapShot, loading3] = useCollection(q2)
  const name = userSnapShot?.docs?.[0]?.data()?.name
  const imageURL = userSnapShot?.docs?.[0]?.data()?.imageURL
  const online = userSnapShot?.docs?.[0]?.data()?.online
  const lastSeen = userSnapShot?.docs?.[0]?.data()?.lastSeen
  const newDate = new Date(lastSeen?.seconds * 1000)
  const time = newDate.toLocaleTimeString().slice(0, 5)
  const date = newDate.toLocaleDateString()

  return (
    <div className='gradient w-full h-screen overflow-hidden'>
      <div className='w-full p-5 bg-[#00000044] backdrop-blur-sm flex items-center space-x-5'>
        <div>
          <Image src={imageURL || DefaultImage} className="rounded-full" width={70} height={70} property="true" quality={100} alt="" />
        </div>
        <div>
          <p>{name}</p>
          {
            online ? <p>online</p> : <p>last seen at {time || ""} on {date || ""}</p>
          }
        </div>
      </div>
      <div className="w-full h-[80vh] overflow-y-auto scroll-bar overflow-x-hidden p-5">
        {
          messageSnapshots?.docs?.map(item => {
            if (item.data().chatId === id) {
              return (
                <div key={item.id} className={(item.data().user === user?.email) ? ("w-full flex justify-end mb-5") : ("w-full flex mb-5")}>
                  <Message msg={item} />
                </div>
              )
            }
          })
        }
      </div>
      <form onSubmit={createMessage} className='w-full p-5 bg-[#00000044] backdrop-blur-sm h-full'>
        <div className='flex items-center relative'>
          <input type="text" onChange={(e) => { setMessage(e.target.value) }} value={message} required placeholder='Type here' className='w-full border pr-[60px] pl-5 py-2 bg-transparent rounded-full focus:border-[#cd71ff]' />
          <button className='text-3xl absolute right-4'><MdSend /></button>
        </div>
      </form>
    </div>
  )
}

export default ID
